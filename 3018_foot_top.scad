use <exponential-column.scad>;

use <parts/2020-latch.scad>;
use <parts/jigsaw_puzzle.scad>;

dim = [21.8, 30, 95];
jigsaw_z = 10;
bottom_scale = 1.5;

difference() {
    scale([1, dim.y/dim.x, 1]) {
        exponential_column(
            h=dim.z,
            d1=dim.x*bottom_scale,
            d2=dim.x,
            steps=100,
            $fn=32);
    }

    translate([0, 0, dim.z]) {
        rotate([-90, 0, 0]) {
            translate([-dim.x/2, 0, -dim.y]) {
                linear_extrude(height=dim.y*2) {
                    offset(r=0.1) {
                        polygon(jigsaw_points([dim.x, jigsaw_z]));
                    }
                }
            }
        }
    }
}

translate([40, 0, 0]) {
    rotate([0, 0, 180]) {
        2020_latch(l=dim.y);
    }

    intersection() {
        linear_extrude(height=dim.y) {
            translate([-dim.x/2, 1]) {
                polygon([[0, -1], each jigsaw_points([dim.x, jigsaw_z]), [dim.x, -1]]);
            }
        }

        translate([0, 0, dim.y/2]) {
            rotate([-90, 0, 0]) {
                scale([1, dim.y/dim.x, 1]) {
                    cylinder(h=jigsaw_z+1, d=dim.x, $fn=32);
                }
            }
        }
    }
}
