depth = 12;
width = 40;
height = 61.5;
hook_d = 24;
hook_ext = 7;
thickness = 2;
bottom_dy = 2;
dist = 8;
dist_dy = 8;

dy = (height-width)/2;
dx = -width/2-hook_d/2;


module outline() {
    module hook(diameter) {
        difference() {
            union() {
                circle(d=diameter, $fn=48);
                translate([-diameter/2, 0]) {
                    square([diameter, hook_ext]);
                }
            }
            translate([-diameter/2, hook_ext]) {
                square(diameter);
            }
        }
    }

    difference() {
        union() {
            hull() {
                for (y=[-dy, dy]) {
                    translate([0, y]) {
                        circle(d=width+2*thickness, $fn=92);
                    }
                }
            }

            hull() {
                for (x=[-dx, dx]) {
                    translate([x, -dy]) {
                        hook(hook_d);
                    }
                }
                translate([0, -dy]) {
                    difference() {
                        circle(d=width+2*thickness, $fn=92);
                        translate([0, width/2]) {
                            square(width+2*thickness, center=true);
                        }
                    }
                }
            }
        }

        hull() {
            for (y=[-dy, dy]) {
                translate([0, y]) {
                    circle(d=width, $fn=96);
                }
            }
        }

        for (x=[-dx, dx]) {
            translate([x, -dy]) {
                hook(hook_d - 2*thickness);
            }
        }

        translate([-width, -width*2-dy-hook_d/2 - bottom_dy]) {
            square(width*2);
        }
    }
}

difference() {
    union() {
        linear_extrude(depth) {
            outline();
        }
        linear_extrude(depth+dist) {
            intersection() {
                outline();
                translate([-width/2-thickness-.1, dy+dist_dy]) {
                    square([width+2*thickness+.2, width/2+thickness]);
                }
            }
        }
    }

    round_h = width+hook_d*2+8*thickness;
    translate([0, -dy+hook_ext, depth/2]) {
        rotate([0, 90, 0]){
            difference() {
                cube([depth*2, depth, round_h], center=true);
                translate([0, -depth/2, 0]){
                    cylinder(d=depth, h=round_h+1, center=true, $fn=24);
                }
                cube(width+3*thickness, center=true);
            }
        }
    }

    translate([0, dy+dist_dy, depth+dist]) {
        rotate([0, 90, 0]) {
            cylinder(r=dist, h=width+2*thickness+.4, center=true);
        }
    }
}
