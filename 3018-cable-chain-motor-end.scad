motor_a = 42;
motor_dy = 30;
motor_cable_slot_x = 16.5;
thickness = 1.5;
hat_h = 8;

ccxe_y = 24.68;

/* hook */
module hook (h, r=4) {
    stiffness = r/2;
    width_global = 2*(r+stiffness);

    big_r = 0.4 * r;
    small_r = 0.5 * stiffness;

    translate([-stiffness, -r, 0]) {
        difference() {
            cube([width_global, r, h]);
            translate([r+stiffness, r, -0.1]) {
                cylinder(r=r, h=h+0.2);
            }
        }

        translate([width_global/2, -r, h/2]) {
            difference() {
                intersection() {
                    cube([width_global, 2*r+0.1, h], center=true);
                    translate([-stiffness, r, 0]) {
                        cylinder(r=r+2*stiffness, h=h+0.1, center=true);
                    }
                }

                hull () {
                    translate([-big_r/2-stiffness/4, 0, 0]) {
                        cylinder(r=big_r, h=h+0.1, center=true);
                    }

                    translate ([big_r, stiffness/2, 0]) {
                        cylinder(r=small_r, h=h+0.1, center=true);
                    }
                }
            }
        }

        translate ([1.5*stiffness + 2*r,r,h/2])
            rotate ([0,90,0])
            difference ()
        {
            hull ()
            {
                translate ([h/2-h/10,0,0])
                    cylinder (d=h/5, h=stiffness, center=true);
                translate ([-h/2+h/10,0,0])
                    cylinder (d=h/5, h=stiffness, center=true);

                translate ([h/7,h/5,0])
                    cylinder (d=h/5, h=stiffness, center=true);
                translate ([-h/7,h/5,0])
                    cylinder (d=h/5, h=stiffness, center=true);
            }

            translate ([0,-h/4,0])
                cube ([h+0.1, h/2, h], center=true);

        }

    }
}

difference() {
    union() {
        /* chain end */
        rotate([0, 0, 90]) translate([-0.5 * ccxe_y, 54, 6]) {
            difference() {
                import("vendor/cableChain_Xend.stl", convexity=10);
                for(x=[-4, 4])
                    translate([x-0.5, -42, -10])
                        cylinder(d=3, h=10, $fn=16);
            }
        }

        translate([-thickness, 0, 0]) {
            cube([motor_a+2*thickness, motor_dy+motor_a+thickness, thickness]);
        }

        translate([motor_a+thickness-ccxe_y, 0, 0]) {
            intersection() {
                cylinder(r=ccxe_y, h=thickness);
                translate([0, -ccxe_y, -0.1]) {
                    cube([ccxe_y, ccxe_y, thickness+0.2]);
                }
            }
        }

        translate([-thickness, -ccxe_y, 0]) {
            cube([motor_a + 2*thickness - ccxe_y, ccxe_y+thickness, thickness]);
        }

        hull() {
            translate([-thickness, motor_dy-thickness, 0]) {
                cube([motor_a+2*thickness, motor_a+2*thickness, hat_h]);
            }
            translate([-thickness, motor_dy-thickness-hat_h, 0]) {
                cube([motor_a+2*thickness, thickness+hat_h, thickness]);
            }
        }
    }

    translate([0, motor_dy, thickness]) {
        cube(motor_a);
    }

    translate([(motor_a - motor_cable_slot_x)/2, 0, thickness]) {
        cube([motor_cable_slot_x, motor_dy+motor_a, thickness+hat_h]);
    }

    translate([motor_a/2, motor_dy + motor_a/2, -thickness/2]) {
        cylinder(d=0.4*motor_a, h=thickness*2, $fn=32);
    }

    for (x=[3, motor_a-3], dy=[-4, 4]) {
        translate([x, (motor_dy-thickness-hat_h)/2 + dy, -thickness/2]) {
            cylinder(d=3, h=2*thickness, $fn=16);
        }
    }
}

translate([motor_a+thickness, motor_dy+motor_a, 0]) {
    hook(h=thickness, $fn=32);
}


translate([motor_a+thickness, motor_dy, thickness]) {
    rotate([180, 0, 0]) {
        hook(h=thickness, $fn=32);
    }
}
