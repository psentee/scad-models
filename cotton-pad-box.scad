use <./dotSCAD/src/rounded_cube.scad>;
use <./dotSCAD/src/rounded_square.scad>;

inner_dim = [40, 54, 40];
side_thickness = 0.9;
// bottom_thickness = 1.84;
bottom_thickness = 1.04;
thumb_d = 15;

epsilon = $preview ? 0.01 : 0;

difference() {
    translate([-side_thickness, -side_thickness, -bottom_thickness]) {
        rounded_cube(inner_dim + [2*side_thickness, 2*side_thickness, bottom_thickness+2*side_thickness], corner_r=2*side_thickness, $fn=32);
    }
    translate([-side_thickness-epsilon, -side_thickness-epsilon, inner_dim.z]) {
        cube([inner_dim.x + 2*(side_thickness+epsilon), inner_dim.y + 2*(side_thickness+epsilon), 2*(side_thickness+epsilon)]);
    }
    rounded_cube(inner_dim + [0, 0, bottom_thickness], corner_r=side_thickness, $fn=24);

    hull() {
        translate([inner_dim.x/2, 0, thumb_d]) {
            rotate([90, 0, 0]) {
                cylinder(d=thumb_d, h=3*side_thickness, $fn=48, center=true);
            }
        }

        translate([inner_dim.x/2, 0, inner_dim.z]) {
            cube([thumb_d, 3*side_thickness, bottom_thickness], center=true);
        }
    }

    for (dx=[-thumb_d, thumb_d]) {
        translate([inner_dim.x/2+dx, 0, inner_dim.z-thumb_d/2]) {
            rotate([90, 0, 0]) {
                difference() {
                    translate([dx>0 ? -thumb_d/2 : 0, 0, -1.48*side_thickness]) {
                        cube([thumb_d/2, thumb_d/2, 2.96*side_thickness]);
                    }
                    cylinder(d=thumb_d, h=3*side_thickness, $fn=48, center=true);
                }
            }
        }
    }
}
