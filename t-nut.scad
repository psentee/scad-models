/* Nut for 3018 T-slot table */


module t_nut_outline(
    a=8,                        /* narrow part width */
    e=16,                       /* wide part width */
    f=3,                        /* bottom fillet */
    h=8,                        /* full height */
    k=5,                        /* bottom part height */
    ) {
    xb=e/2;
    xt=a/2;
    polygon([[-xb+f, 0],
             [-xb, f],
             [-xb, k],
             [-xt, k],
             [-xt, h],
             [xt, h],
             [xt, k],
             [xb, k],
             [xb, f],
             [xb-f, 0]]);
}

/* TODO: parameters for outline? */
module t_nut_hex(l, d, d_nut, h_nut, h_total=30) {
    difference() {
        linear_extrude(height=l) {
            t_nut_outline();
        }

        rotate([-90, 0, 0]) {
            translate([0, -l/2, -0.5]) {
                cylinder(d=d, h=h_total, $fn=24);
                cylinder(d=d_nut, h=h_nut+0.5, $fn=6);
            }
        }
    }
}

// M4 nut
t_nut_hex(l=10, d=4.2, d_nut=7.8, h_nut=3);

// M6 screw
// t_nut_hex(d=6.6, d_nut=11.5, h_nut=4.5, l=14);
