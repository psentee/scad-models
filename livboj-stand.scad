phone = [72, 142, 6];
thickness = 2.5;
corner_r=1.2;

livboj_dia_margin = 0.67;
livboj_dia_bottom = 91 + livboj_dia_margin;
livboj_dia_top = 89.953 + livboj_dia_margin;
livboj_height = 10;
livboj_center_y = 70;

voronoi_grid = 10.5;
voronoi_thickness = 1.5;

use <dotSCAD/src/rounded_cube.scad>;
use <dotSCAD/src/rounded_square.scad>;
use <dotSCAD/src/voronoi/vrn2_space.scad>;

$fn=32;

difference() {
    height=thickness + livboj_height + phone.z;

    rounded_cube([phone.x+2*thickness, phone.y+2*thickness, 2*height], corner_r=corner_r+thickness, center=true);

    cut = [phone.x+4*thickness, phone.y+4*thickness, 2*height];
    translate([-cut.x/2, -cut.y/2, 0]) {
        cube(cut);
    }

    translate([0, 0, -phone.z]) {
        linear_extrude(height) {
            rounded_square([phone.x, phone.y], corner_r=corner_r, center=true);
        }
    }

    livboj_dy = livboj_center_y-phone.y/2;
    translate([0, livboj_dy, -phone.z-livboj_height]) {
        cylinder(h=livboj_height+phone.z+1, d1=livboj_dia_bottom, d2=livboj_dia_top, $fn=64);
    }

    translate([0, 0, -height-1]) {
        difference() {
            linear_extrude(height+1) {
                intersection() {
                    difference() {
                        rounded_square([phone.x, phone.y], corner_r=corner_r, center=true);
                        translate([0, livboj_dy]) {
                            difference() {
                                circle(d=livboj_dia_bottom+2*thickness);
                                circle(d=livboj_dia_bottom-2*thickness);
                            }
                        }
                    }

                    voronoi_size = [phone.x+20, phone.y+20];
                    translate(-voronoi_size/2) {
                        vrn2_space(voronoi_size, voronoi_grid, spacing=voronoi_thickness, region_type="circle");
                    }
                }
            }

            linear_extrude(1.6) {
                difference() {
                    delta=thickness;
                    square([phone.x+2*thickness, phone.y+2*thickness], center=true);
                    rounded_square([phone.x-delta, phone.y-delta], corner_r=corner_r, center=true);
                }
            }
        }
    }

    /* snooze cutout */
    snooze = [5*thickness, 10+2+thickness, phone.z*2];
    translate([phone.x/2, 105-phone.y/2, -phone.z+snooze.z/2]) {
        rounded_cube(snooze, corner_r=thickness, center=true);
    }
}
