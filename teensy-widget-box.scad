use <dotSCAD/src/voronoi2d.scad>; /* FIXME: new voronoi */
use <exponential-column.scad>;

// $draft = false;
// $layer = 0.1;
// $draft = true;
// $layer = 1.8;

leg_h = 8;
leg_r = 10;

tie_pos = [57.5, 20];
tie_groove_w = 7.5;
tie_ziphole_d = 3;
tie_d1 = 20;
tie_d2y = tie_groove_w + 2*(tie_ziphole_d + 2);
tie_d2x = 8;
tie_cap_h = 3;
tie_scale = [1, tie_d2y/tie_d2x, 1];

bottom_thickness = 0.7;

drillholes = [
    [5, 5],
    [3, 33.5],
    [44, 6.5],
    [45, 35.5],
];

module m3_leg(h, r1) {
    difference() {
        maybe_exponential_column(r1=r1, r2=4, h=h, $fn=48);
        translate([0, 0, -0.1]) {
            cylinder(d=4.2, h=h+0.2, $fn=16);
            cylinder(d=6.67, h=h-1.1, $fn=6);
        }
    }
}

module cable_tie() {
    translate(tie_pos) {
        difference() {
            scale(tie_scale) {
                difference() {
                    maybe_exponential_column(h=leg_h+2, d1=tie_d1, d2=tie_d2x, $fn=48);
                    translate([0, 0, -0.1]) {
                        cylinder(d=tie_d2x-1, h=leg_h+0.1, $fn=32);
                    }
                }
            }

            /* cable groove */
            translate([0, 0, 1+(leg_h+2-0.5)]) {
                cube([10, tie_groove_w, 2], center=true);
            }

            /* zip tie holes */
            for ( y=[-0.5, 0.5] ) {
                translate([0, y*(tie_groove_w+tie_ziphole_d+0.1), 0]) {
                    cylinder(d=tie_ziphole_d, h=leg_h+10, $fn=8);
                }
            }
        }
    }
}

module legs() {
    for (pos=drillholes) {
        translate(pos) {
            m3_leg(h=leg_h, r1=leg_r);
        }
    }
    cable_tie();
}

module bottom() {
    difference() {
        hull() {
            intersection() {
                legs();
                translate([-10, -10, 0]) {
                    cube([80, 80, bottom_thickness]);
                }
            }
        }
        for (pos=drillholes) {
            translate([pos.x, pos.y, -0.05]) {
                cylinder(d=6.5, h=bottom_thickness+0.1);
            }
        }

        translate([tie_pos.x, tie_pos.y, -0.05]) {
            resize([tie_d2x+2, tie_d2y+2, bottom_thickness+0.1]) {
                cylinder(d=10, h=1);
            }
        }
    }
}

difference() {
    union() {
        legs();
        bottom();
    }
    /* voronoi */
    points = [for(i=[0:150]) rands(-25, 75, 2)];
    translate([0, 0, -0.1]) {
        linear_extrude(height=leg_h) {
            intersection() {
                voronoi2d(points, spacing=1, chamfer=1, r=-0.5);
                offset(r=-3.5, chamfer=true) projection(cut=false) {
                    bottom();
                }
            }
        }
    }
}

/* zip tie cap */
translate(
    $preview
    ? [tie_pos.x, tie_pos.y, leg_h+2.25] /* preview exploded */
    : [tie_pos.x + 15, tie_pos.y, 0]     /* render for printout */
    ) {
    difference() {
        resize([tie_d2x, tie_d2y, tie_cap_h*2]) {
            sphere(r=10, $fn=64);
        }
        translate([0, 0, -tie_cap_h]) {
            cube([tie_d2x+5, tie_d2y+5, tie_cap_h*2], center=true);
        }
        for ( y=[-0.5, 0.5] ) {
            translate([0, y*(tie_groove_w+tie_ziphole_d), -0.5]) {
                cylinder(d=tie_ziphole_d, h=10, $fn=8);
            }
        }

        translate([0, 0, tie_cap_h-0.5]) {
            cube([tie_ziphole_d, tie_groove_w+tie_ziphole_d, 1], center=true);
        }
    }
}
