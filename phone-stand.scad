// phone = [72, 142, 6]; // iphone 8
phone = [82.5, 164.5, 11]; // iphone 13 pro max
thickness = 1.8;
corner_r = 5; // 1.2;
notch_corner_r = 2;
notch_bottom = 2;
angle = 30;
step_h = 8;

camera = [45, 45, 2.6];

livboj = false;
livboj_dia_margin = 0.33;
livboj_dia_bottom = 91 + livboj_dia_margin;
livboj_dia_top = 89.953 + livboj_dia_margin;
livboj_height = 10;
// livboj_center_y = 70; // iphone 8
livboj_center_y = 82; // iphone 13 pro max

use <parts/notch.scad>;

outer = phone + [
    2*thickness,
    2*thickness + (phone.z + camera.z) * tan(angle),
    camera.z + (livboj ? livboj_height : 0) + thickness,
    ];

y1 = outer.y*cos(angle);
z0 = outer.z*cos(angle) + step_h;
z1 = outer.y*sin(angle);

module outer_hull(corner_r) {
    hull() {
        for (x = [corner_r, outer.x-corner_r]) {
            for (yz = [
                     [corner_r, corner_r],
                     [y1-corner_r, corner_r],
                     [y1-corner_r, z0+z1],
                     [corner_r, z0],
                     /* [corner_r, corner_r], */
                     /* [y1-corner_r, corner_r], */
                     /* [y1-corner_r, z0+z1], */
                     /* [corner_r, z0], */
                     ]) {
                translate([x, yz[0], yz[1]]) {
                    sphere(r=corner_r);
                }
            }
        }

        for (x = [-(phone.y - phone.x)/2 + corner_r, (phone.y+phone.x)/2 - corner_r]) {

            for (yyy = [-1, 1]) {
                yy = livboj_center_y + yyy*phone.x/2 - yyy*corner_r;
                 translate([x, yy*cos(angle), z0+yy*sin(angle)]) {
                    sphere(r=corner_r);
                }
            }
        }
    }
}

module phone_cutout() {
    linear_extrude(phone.z+1) {
        hull() {
            for (pos = [
                     [corner_r, corner_r],
                     [phone.x-corner_r, corner_r],
                     [phone.x-corner_r, phone.y-corner_r],
                     [corner_r, phone.y-corner_r],
                     ]) {
                translate(pos) {
                    circle(r=corner_r, $fn=32);
                }
            }
        }
    }

    /* notches */
    translate([0, 0, notch_bottom]) {
        notch_th = thickness+10;
        notch_z = phone.z - notch_bottom;
        notch_offset = 0.1 - notch_th/2;

        /* charging port + speakers */
        translate([phone.x/2, notch_offset, 0]) {
            notch_xz([55, notch_th, notch_z], corner_r=notch_corner_r, margin=1, $fn=16);
        }

        /* right button */
        right_notch_dy = 0; // 2;
        translate([phone.x-notch_offset, 110.5 - right_notch_dy, 0]) {
            notch_yz([notch_th, 23 + right_notch_dy/2, notch_z], corner_r=notch_corner_r, margin=1, $fn=16);
        }

        /* left buttons */
        left_notch_dy = 0; // 5;
        translate([notch_offset, 116.5 - left_notch_dy/2, 0]) {
            notch_yz([notch_th, 40 + left_notch_dy/2, notch_z], corner_r=notch_corner_r, margin=1);
        }
    }

    /* camera hole */
    translate([0, 0, -camera.z]) {
        linear_extrude(camera.z+1) {
            hull() {
                for (pos = [
                         [phone.x-camera.x+corner_r, phone.y-camera.y+corner_r],
                         [phone.x-corner_r, phone.y-camera.y+corner_r],
                         [phone.x-corner_r, phone.y-corner_r],
                         [phone.x-camera.x+corner_r, phone.y-corner_r],
                         ]) {
                    translate(pos) {
                        circle(r=corner_r, $fn=32);
                    }
                }
            }
        }
    }

    if (livboj) {
        /* charger hole */
        translate([phone.x/2, livboj_center_y, -livboj_height]) {
            cylinder(h=livboj_height, d1=livboj_dia_bottom, d2=livboj_dia_top, $fn=64);
            cylinder(h=livboj_height+phone.z+1, d=livboj_dia_top, $fn=64);
        }
    }
}

difference() {
    intersection() {
        !outer_hull(corner_r=corner_r+thickness, $fn = 32);
        outer_hull(corner_r=0.1);
    }

    translate([0, 0, z0]) rotate([angle, 0, 0]) translate([thickness, thickness, -phone.z]) phone_cutout();
}
