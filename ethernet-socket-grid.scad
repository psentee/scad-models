use <dotSCAD/src/voronoi/vrn2_space.scad>;

use <./ethernet-socket-cover.scad>;

$fn=12;

linear_extrude(0.64) {
    difference() {
        frame_inside();
        difference() {
            translate([-30, -30]) {
                vrn2_space([60, 60], grid_w=6, spacing=3.4, r=1, $fn=12);
            }
            square([80, 2], center=true);
            square([2, 80], center=true);
            translate([-24, 0]) square([2, 50], center=true);
            translate([24, 0])  square([2, 50], center=true);
            translate([0, -24]) square([50, 2], center=true);
            translate([0, 24])  square([50, 2], center=true);
        }
        /* for (x=[-27.5:2.5:27.5], y=[-27.5:2.5:27.5]) { */
        /*     translate([x, y]) { */
        /*         circle(d= x*y==0 ? 2 : 1.5); */
        /*     } */
        /* } */
    }
}

linear_extrude(4) {
    difference() {
        frame_inside();
        offset(r=-2.7) {
            frame_inside();
        }
    }
}
