use <dotSCAD/src/rounded_square.scad>;
use <dotSCAD/src/rounded_cube.scad>;
use <dotSCAD/src/voronoi/vrn2_space.scad>;

inner_dim = [80, 45, 100];                 /* inner dimension */
n_rods = [2, 1];
rod_d = 10.67;
rod_margin = 4.2;

side_mounted = false;

_epsilon = 0.01;

function _rod_positions(
    inner_dim = inner_dim,
    n_rods = n_rods,
    rod_d = rod_d,
    rod_margin = rod_margin,
    no_vertical = false,
    no_horizontal = false
    ) =
    let(
        rod_dx = inner_dim.x / (n_rods.x + 1),
        rod_dy = inner_dim.y / (n_rods.y + 1)
        )
    concat(
        no_horizontal ? [] : [ for (i=[1:n_rods.x]) [rod_dx*i, rod_d/2 + rod_margin] ],
        no_vertical ? [] : [ for (i=[1:n_rods.y]) [inner_dim.x + rod_d/2, rod_dy*i + rod_d/2 + rod_margin] ]
            );

module tea_rack(
    inner_dim = inner_dim,
    n_rods = n_rods,
    rod_d = rod_d,
    rod_margin = rod_margin,
    thickness = 10,
    thinness = 1,

    /* TODO: magnet vs screw */
    mount_n = 3,                /* ignored when not side_mount */
    mount_margin,               /* default to rod_margin */
    mount_type = "screw",       /* "screw" or "magnet" */

    screw_d = 3.4,
    screwhead_d = 6.5,
    screw_thickness = 2,

    magnet_d = 8.24,
    magnet_h = 6.3,
    magnet_thickness = 0.8,
    magnet_toothpick_d = 2,

    slide_r = 3.4,
    round_r = 1.6,

    debug = true
) {
    /* checks */
    assert(mount_type == "screw" || mount_type == "magnet");

    /* computed defaults */
    mount_margin = mount_margin == undef ? rod_margin : mount_margin;

    /* computed values */
    magnet_p = mount_type == "magnet";
    rod_positions = _rod_positions(inner_dim, n_rods, rod_d, rod_margin);
    rod_hull_d = rod_d+2*rod_margin;
    rod_yy = [for (pos = rod_positions) pos.y];
    assert(abs(min(rod_yy) - rod_hull_d/2) < 0.001, "side y does not start at 0???");
    side_ymax = max(rod_yy) + rod_hull_d/2;

    back_thickness = magnet_p ? magnet_h+magnet_thickness : screw_thickness + (screwhead_d - screw_d)/2 + 0.8;

    mount_d = magnet_p ? magnet_d : screwhead_d;
    mount_z = side_mounted ? thickness + slide_r + mount_d/2 : thickness/2;
    mount_dy = (side_ymax - 2 * rod_margin - mount_d) / (mount_n - 1);
    mount_yy =  side_mounted ? [rod_margin + mount_d/2 : mount_dy : side_ymax - (rod_margin + mount_d/2)] : [-thickness/2, side_ymax+thickness/2];

    module back_hull(length) {
            hull() {
                cube([length, side_ymax, thickness]);

                for (y = mount_yy) {
                    translate([0, y, mount_z]) {
                        rotate([0, 90, 0]) {
                            cylinder(d=mount_d + mount_margin, h=length, $fn=32);
                        }
                    }
                }
            }
    }

    module side_hull(height) {
        linear_extrude(height) {
            hull() {
                for (pos = rod_positions) {
                    translate(pos) {
                        circle(d=rod_hull_d, $fn=32);
                    }
                }

                // square([_epsilon, side_ymax]);
                for (y = [round_r, side_ymax-round_r]) {
                    translate([round_r, y]) {
                        circle(r=round_r, $fn=24);
                    }
                }
            }
        }
    }

    difference() {
        union() {
            side_hull(thickness);

            if (side_mounted) {
                intersection() {
                    back_hull(back_thickness);
                    side_hull(mount_z + mount_d + mount_margin);
                }


                /* slide */
                intersection() {
                    translate([back_thickness - _epsilon, 0, thickness - _epsilon]) {
                        difference() {
                            cube([slide_r + _epsilon, side_ymax, slide_r + _epsilon]);
                            translate([slide_r + _epsilon, -_epsilon, slide_r + _epsilon]) {
                                rotate([-90, 0, 0]) {
                                    cylinder(r=slide_r, h=side_ymax+2*_epsilon, $fn=24);
                                }
                            }
                        }
                    }
                    back_hull(back_thickness + slide_r + 5);
                }
            } else {
                assert(thickness >= mount_d + 1.6);

                translate([0, -thickness, thickness]) {
                    rotate([0, 90, 0]) {
                        linear_extrude(height = back_thickness) {
                            rounded_square([thickness, side_ymax+2*thickness], corner_r=thickness/2, $fn=24);
                        }
                    }
                }

                difference() {
                    translate([back_thickness-_epsilon, -slide_r, 0]) {
                        cube([slide_r+_epsilon, slide_r+_epsilon, thickness]);
                    }
                    translate([back_thickness+slide_r, -slide_r, -_epsilon]) {
                        cylinder(r=slide_r, h=thickness+2*_epsilon, $fn=24);
                    }
                }

                difference() {
                    translate([back_thickness-_epsilon, side_ymax-_epsilon, 0]) {
                        cube([slide_r+_epsilon, slide_r+_epsilon, thickness]);
                    }
                    translate([back_thickness+slide_r, side_ymax+slide_r, -_epsilon]) {
                        cylinder(r=slide_r, h=thickness+2*_epsilon, $fn=24);
                    }
                }
            }
        }

        /* rod holes */
        for (pos = rod_positions) {
            translate([pos.x, pos.y, side_mounted ? -_epsilon : thinness]) {
                cylinder(d=rod_d, h=thickness-thinness+_epsilon, $fn=32);
            }
        }

        /* mount holes */
        for (y = mount_yy) {
            translate([0, y, mount_z]) {
                rotate([0, 90, 0]) {
                    if (magnet_p) {
                        translate([0, 0, magnet_thickness]) {
                            cylinder(d=magnet_d, h=back_thickness+slide_r, $fn=32);
                        }
                        if (magnet_toothpick_d > 0) {
                            translate([0, 0, -_epsilon]) {
                                cylinder(d=magnet_toothpick_d, h=back_thickness, $fn=24);
                            }
                        }
                    } else {
                        translate([0, 0, -_epsilon]) {
                            cylinder(d=screw_d, h=back_thickness+slide_r+_epsilon, $fn=24);
                        }
                        translate([0, 0, screw_thickness - screw_d/2]) {
                            cylinder(d1=0, d2=screwhead_d, h=screwhead_d/2, $fn=24);
                        }
                        translate([0, 0, screw_thickness + (screwhead_d - screw_d)/2 - 0.1*_epsilon]) {
                            cylinder(d=screwhead_d, h=back_thickness+slide_r, $fn=24);
                        }
                    }
                }
            }
        }
    }
}

module bottom(
    inner_dim = inner_dim,
    n_rods = n_rods,
    rod_d = rod_d,
    rod_margin = rod_margin,
    margin_d = 0.6,
    thickness = 3,
    thinness = 0.64,
    corner_r = 10,
    border = 3
    ) {
    rod_xx = [for (pos = _rod_positions(inner_dim, n_rods, rod_d, rod_margin, no_vertical = true)) pos.x];

    module outline() {
        rounded_square([inner_dim.x, inner_dim.z], corner_r=corner_r);
    }

    difference() {
        linear_extrude(thickness) {
            difference() {
                outline();
                intersection() {
                    offset(r=-border) {
                        outline();
                    }
                    vrn2_space([inner_dim.x, inner_dim.z], grid_w=10, region_type="circle");
                }
            }
        }
        for (x = rod_xx) {
            translate([x, -_epsilon, rod_d/2 + thinness]) {
                rotate([-90, 0, 0]) {
                    cylinder(d=rod_d + margin_d, h=inner_dim.z+2*_epsilon, $fn=32);
                }
            }
        }
    }
}

// bottom();
tea_rack();

