use <exponential-column.scad>;

height = 50;
dh = 3;

module outline() {
    offset(r=0.67) projection(cut=false) intersection() {
        translate([0, -70, 0]) import("vendor/Tripod_base.stl", convexity=5);
        cylinder(r=15, h=5);
    }
}
// outline();

difference() {
    maybe_exponential_column(r1=32, r2=12, h=height+dh, $layer=1);
    translate([0, 0, height]) {
        linear_extrude(height=dh+1) {
            outline();
        }
    }
}

