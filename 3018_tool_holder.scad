// use <rounded_cube.scad>;

dim = [50, 15, 30];
wrench_slot_dim = [60, 2.5, 14];

dd = [5.5, 4.5, 3.5, 3];

module imbus(d) {
    knee_r = d;
    rotate([90, 0, -90]) translate([-knee_r, -knee_r, 0]) {
        rotate_extrude(angle=90) {
            translate([knee_r, 0]) {
                circle(d=d, $fn=16);
            }
        }

        translate([knee_r, knee_r+(d/2), 0]) rotate([90, 0, 0]) cylinder(d=d, h=64+knee_r+d/2, $fn=16);
        translate([d, knee_r, 0]) rotate([0, -90, 0]) cylinder(d=d, h=17+d, $fn=16);

        translate([0, 0, -d/2]) {
            difference() {
                cube(d);
                translate([0, 0, -0.5])
                    cylinder(r=d, h=d+1, $fn=16);
            }
        }
    }
}

difference() {
    cube(dim, center=true);

    y0 = -(dim.y - wrench_slot_dim.y) / 2;

    translate([0, y0+1, 0]) {
        rotate([0, 30, 0]) {
            cube(wrench_slot_dim, center=true);
        }
    }

    translate([0, y0+wrench_slot_dim.y+2, 0]) {
        rotate([0, -30, 0]) {
            cube(wrench_slot_dim, center=true);
        }
    }

    for(i=[0:len(dd)-1]) {
        d = dd[i];
        translate([dim.x/2-10-i*10, dim.y/2-5, dim.z/2+d/2]) {
            rotate([-20, 30, -45]) {
                imbus(d);
            }
        }
    }
}

translate([0, 1-dim.y/2, 0]) {
    rotate([90, 0, 0]) {
        linear_extrude(height=1) {
            difference() {
                hull() {
                    for (i=[-0.5, 0.5]) {
                        translate(i*[dim.x+10, dim.z-10]) {
                            circle(d=10, $fn=16);
                        }
                    }
                    square([dim.x, dim.z], center=true);
                }
                for (i=[-0.5, 0.5]) {
                    translate(i*[dim.x+10, dim.z-10]) {
                        circle(d=4.2, $fn=12);
                    }
                }
            }
        }
    }
}
