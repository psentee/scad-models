outest_d = 12;
outer_d = 4;
inner_d = 2.2;
mid_distance = 12.08 / 2;
depth = 0.8;
height = 14.04;

difference() {
    union() {
        linear_extrude(height) {
            hull() {
                for (x = [-mid_distance, mid_distance]) {
                    translate([x, 0]) {
                        circle(d=outest_d, $fn=24);
                    }
                }
            }
        }
        translate([0, 0, height-0.01]) {
            linear_extrude(depth+0.01) {
                hull() {
                    for (x = [-mid_distance, mid_distance]) {
                        translate([x, 0]) {
                            circle(d=outer_d, $fn=24);
                        }
                    }
                }
            }
        }
    }

    for (x = [-mid_distance, 0, mid_distance]) {
        translate([x, 0, -0.01]) {
            cylinder(d=inner_d, h=height+depth+0.02, $fn=24);
        }
    }
}
