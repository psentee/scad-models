use <./dotSCAD/src/rounded_square.scad>;
use <./dotSCAD/src/util/reverse.scad>;
use <./parts/jigsaw_puzzle.scad>;

module modular_box(
    inner_dim,
    compartments = [[], []],
    thickness = 1.2,
    bottom_thickness = 1.6,
    connector_dim = [20, 6.5], /* x is along the edge, y is perpendicular */
    connector_precision = 0.16,
    corner_r = 3.2,
    divider_dh = 10,
) {

/* TODO: rounded bottom? smoother shape in general? */

    connector_points = jigsaw_points(connector_dim);
    connector_dx = (inner_dim.x - connector_dim.x) / 2;
    connector_dy = (inner_dim.y - connector_dim.x) / 2;

    module connector_outer() { polygon(connector_points); }
    module connector_inner() { offset(r=connector_precision) polygon(connector_points); }
    module connector_pad() { offset(r=thickness) { hull() { connector_inner(); } } }

    module external_outline() {
        union() {
            difference() {
                rounded_square([inner_dim.x+2*thickness, inner_dim.y + 2 * thickness], corner_r=corner_r, $fn=24);
                translate([(inner_dim.x - connector_dim.x)/2 + thickness, 0]) connector_inner();
                translate([0, (inner_dim.y + connector_dim.x)/2 + thickness]) rotate(-90) connector_inner();
            }
            translate([(inner_dim.x - connector_dim.x)/2 + thickness, inner_dim.y + 2 * thickness]) connector_outer();
            translate([inner_dim.x + 2 * thickness, (inner_dim.y + connector_dim.x)/2 + thickness]) rotate(-90) connector_outer();
        }
    }

    difference() {
        translate([0, 0, -bottom_thickness]) linear_extrude(height=inner_dim.z + bottom_thickness) external_outline();
        difference() {
            linear_extrude(height=inner_dim.z + thickness) {
                difference() {
                    intersection() {
                        offset(r=-thickness) external_outline();
                        translate([thickness, thickness]) square([inner_dim.x, inner_dim.y]);
                    }
                    translate([(inner_dim.x - connector_dim.x)/2 + thickness, 0]) connector_pad();
                    translate([0, (inner_dim.y + connector_dim.x)/2 + thickness]) rotate(-90) connector_pad();
                }
            }

            linear_extrude(height = inner_dim.z - divider_dh) {
                if (len(compartments.y) > 0) {
                    for (y = [ for (i = 0, cy = compartments.y[0] + thickness ; i < len(compartments.y) ; i = i + 1, cy = cy + compartments.y[i] + thickness) cy ]) {
                        echo(y);
                        translate([0, inner_dim.y + thickness - y]) {
                            square([inner_dim.x + 2 * thickness, thickness]);
                        }
                    }
                }

                if (len(compartments.x) > 0) {
                    for (x = [ for (i = 0, cx = compartments.x[0] + thickness ; i < len(compartments.x) ; i = i + 1, cx = cx + compartments.x[i] + thickness) cx ]) {
                        translate([inner_dim.x + thickness - x, 0]) {
                            square([thickness, inner_dim.y + 2 * thickness]);
                        }
                    }
                }
            }
        }
    }
}

// modular_box([20, 60, 60], compartments = [[], [20, 10]]);
// modular_box([30, 40, 75]);
modular_box([60, 40, 45 - 0.4 /* account for thickness/bottom_thickness misunderstanding */ ], compartments = [[15, 28], [28]], divider_dh=5);
