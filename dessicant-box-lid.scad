use <dotSCAD/src/voronoi2d.scad>; /* FIXME: new voronoi */

thickness = 1;
diameter = 50;

union() {
    linear_extrude(height=thickness) {
        difference() {
            circle(d=diameter+4*thickness, $fn=64);
            intersection() {
                n = 100;
                xx = rands(-0.67*diameter, 0.67*diameter, n);
                yy = rands(-0.67*diameter, 0.67*diameter, n);
                points = [for(i=[0:n-1]) [xx[i], yy[i]]];
                voronoi2d(points, spacing=1, region_type="circle");
                circle(d=diameter-(1+2*thickness), $fn=64);
            }
        }
    }

    linear_extrude(height=thickness + 2.5) {
        difference() {
            circle(d=diameter, $fn=64);
            circle(d=diameter-2*thickness, $fn=64);
        }
    }
}
