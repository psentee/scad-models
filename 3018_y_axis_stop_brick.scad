dx = 13;
slot = [30, 8, 1.5];
dim = [slot.x, 16, 5];

screwhole_d = 4.2;
screwhole_x = [7.5, 22.5];

overlap = 0.5;

difference() {
    union() {
        cube(dim+[dx, 0, 0]);
        translate([dim.x, 0, dim.z-overlap]) {
            hull() {
                cube([dx, dim.y, overlap]);
                cube([1,  dim.y, dim.z+overlap]);
            }
        }
        translate([0, (dim.y-slot.y)/2, dim.z-overlap]) {
            cube([slot.x, slot.y, slot.z+overlap]);
        }
    }
    for(x=screwhole_x) {
        translate([x, dim.y/2, -overlap]) {
            cylinder(d=screwhole_d, h=slot.z + dim.z + 2*overlap, $fn=16);
        }
    }
}
