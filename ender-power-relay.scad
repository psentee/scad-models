use <dotSCAD/src/voronoi2d.scad>; /* FIXME: new voronoi */
use <exponential-column.scad>;

// $draft = false;
// $layer = 0.1;
// $draft = true;
// $layer = 2;

thickness = 1;

leg_h = 4.5 - thickness;
leg_d = 7;
leg_bottom_d = 16;

zip_tie_hole = [1.5, 3.2];

relay_screwholes_dim = [33.5, 44];
relay_screwholes_pos = [
    for (xx=[-0.5, 0.5], yy=[-0.5, 0.5]) [xx*relay_screwholes_dim.x, yy*relay_screwholes_dim.y]
    ];

cube_pos = [45, 22];
cube_h = 6.5 - thickness;
cube_d = 10;
cube_bottom_d = 20;
cube_scale = [1.6, 1];
cube_ziphole_dim = [14, 6, 5.1];

cable_in_pos = [
    for (dxy=[[8, -30], [-8, 30]]) [cube_pos.x+dxy.x, cube_pos.y+dxy.y]
    ];

cable_d = 7;
cable_leg_d = cable_d*2;
cable_leg_bottom_d = cable_d*3;
cable_leg_scale = [1.5, 1];

_cable_screwhole_dx = 2.5*cable_d;
_cable_screwhole_dy = 1.5*cable_d;
screwhole_pos = [
    [cable_in_pos[0].x + _cable_screwhole_dx, cable_in_pos[0].y - _cable_screwhole_dy],
    [cable_in_pos[0].x - _cable_screwhole_dx, cable_in_pos[0].y - _cable_screwhole_dy],
    [cable_in_pos[1].x + _cable_screwhole_dx, cable_in_pos[1].y + _cable_screwhole_dy],
    [cable_in_pos[1].x - _cable_screwhole_dx, cable_in_pos[1].y + _cable_screwhole_dy],
    [cube_pos.x + 23, cube_pos.y],
    [0, -40],
    [-relay_screwholes_dim.x/2-5, relay_screwholes_dim.y/2+17],
    ];
screwhole_d=4;
screwhole_leg_d=6;
screwhole_leg_bottom_d=12;

ziptie_dx = 0.25;
ziptie_xx = [-cable_d/2-zip_tie_hole.x+ziptie_dx, cable_d/2-ziptie_dx];
ziptie_yy = [-zip_tie_hole.y-zip_tie_hole.y/2, zip_tie_hole.y-zip_tie_hole.y/2];
ziphole_dim = [
    abs(ziptie_xx[1] - ziptie_xx[0]) + zip_tie_hole.x + 5,
    abs(ziptie_yy[1] - ziptie_yy[0]) + zip_tie_hole.y + 1,
    5,
    ];


module legs() {
    /* nóżki pod przekaźnik */
    for (pos=relay_screwholes_pos) {
        translate(pos) {
            difference() {
                maybe_exponential_column(d1=leg_bottom_d, d2=leg_d, h=leg_h, $fn=48);
                translate([0, 0, -0.1]) {
                    cylinder(d=4, h=leg_h+0.2, $fn=32);
                }
            }
        }
    }

    /* kostka */
    translate(cube_pos) {
        difference() {
            scale(cube_scale) {
                maybe_exponential_column(d1=cube_bottom_d, d2=cube_d, h=cube_h, $fn=48);
            }

            for (x=[-4, 4]) {
                translate([x-zip_tie_hole.x/2, -zip_tie_hole.y/2, -0.1]) {
                    cube([zip_tie_hole.x, zip_tie_hole.y, 9]);
                }
            }

            translate([-cube_ziphole_dim.x/2, -cube_ziphole_dim.y/2, -thickness]) {
                cube(cube_ziphole_dim);
            }
        }
    }

    /* kabel we/wy */
    for (pos=cable_in_pos) {
        translate(pos) {
            difference() {
                scale(cable_leg_scale) {
                    maybe_exponential_column(d1=cable_leg_bottom_d,
                                             d2=cable_leg_d,
                                             h=cube_h+cable_d/3 - thickness,
                                             $fn=32);
                }
                /* groove */
                translate([0, 0, cube_h+cable_d/2]) {
                    rotate([90, 0, 0]) {
                        cylinder(d=cable_d, h=20, center=true, $fn=32);
                    }
                }
                /* zip tie slots */
                for (x=ziptie_xx, y=ziptie_yy) {
                    translate([x, y, -0.1]) {
                        cube([zip_tie_hole.x, zip_tie_hole.y, cube_h+cable_d]);
                    }
                }
                /* zip tie ending hole */
                translate([-ziphole_dim.x/2, -ziphole_dim.y/2, -thickness]) {
                    cube(ziphole_dim);
                }
            }
        }
    }

    /* śruby */
    for (pos=screwhole_pos) {
        translate(pos) {
            difference() {
                h=5;
                maybe_exponential_column(d1=screwhole_leg_bottom_d, d2=screwhole_leg_d, h=h, $fn=32);
                translate([0, 0, -0.1]) {
                    cylinder(d=screwhole_d, h=h+0.2, $fn=24);
                }
                translate([0,0,h-screwhole_leg_d/2]) {
                    cylinder(d1=0, d2=2+screwhole_leg_d, h=1+screwhole_leg_d/2, $fn=24);
                }
            }
        }
    }
}

module bottom_solids() {
    for (pos=relay_screwholes_pos) {
        translate(pos) {
            circle(d=leg_bottom_d, $fn=48);
        }
    }
    translate(cube_pos) {
        scale(cube_scale) {
            circle(d=cube_bottom_d, $fn=48);
        }
    }
    for (pos=cable_in_pos) {
        translate(pos) {
            scale(cable_leg_scale) {
                circle(d=cable_leg_bottom_d, $fn=32);
            }
        }
    }
    for (pos=screwhole_pos) {
        translate(pos) {
            circle(d=screwhole_leg_bottom_d, $fn=24);
        }
    }
}

module bottom_hull() {
    hull() {
        bottom_solids();
    }
}

module bottom_voronoi () {
    points = [for(i=[0:150]) rands(-50, 100, 2)];
    intersection() {
        offset(r=-4, chamfer=true) {
            difference() {
                bottom_hull();
                bottom_solids();
            }
        }
        voronoi2d(points, spacing=2, chamfer=1.5, r=-0.55);
    }
}

legs();

translate([0,0,-thickness]) {
    difference() {
        linear_extrude(height=thickness+0.01) {
            difference() {
                bottom_hull();
                bottom_voronoi();
            }
        }

        h=thickness*3;

        for (pos=cable_in_pos) {
            translate(pos) {
                cube(ziphole_dim, center=true);
            }
        }

        translate(cube_pos) {
            cube(cube_ziphole_dim, center=true);
        }

        for (pos=relay_screwholes_pos) {
            translate(pos) {
                cylinder(d=4.75, h=h, center=true, $fn=32);
            }
        }

        for (pos=screwhole_pos) {
            translate(pos) {
                cylinder(d=4, h=h, center=true, $fn=24);
            }
        }
    }
}
