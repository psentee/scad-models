/* TODO: fillet the cantilever? */

module 2020_latch(
    l,                          /* length */
    th=0.8,                     /* thickness */
    tol=0.1,                    /* tolerance */
    ld=1.2,                     /* latch depth */
    ll=5,                       /* latch length */
) {
    translate([-10, 0, 0]) {
        linear_extrude(height=l) {
            polygon([[-th-tol, -th],
                     [-th-tol, 7+ll],
                     [ld, 7+tol],
                     [-tol, 7+tol],
                     [-tol, 0],
                     [7+tol, 0],
                     [7+tol, 5],
                     [13-tol, 5],
                     [13-tol, 0],
                     [20+tol, 0],
                     [20+tol, 7+tol],
                     [20-ld, 7+tol],
                     [20+th+tol, 7+ll],
                     [20+th+tol, -th]]);
        }
    }
}
