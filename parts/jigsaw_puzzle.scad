use <../dotSCAD/src/bspline_curve.scad>;
// use <polyline2d.scad>;

// use <./2020-latch.scad>;
use <../dotSCAD/src/rounded_square.scad>;

_jigsaw_points_1 = [
    [0, 0],
    [0, 0],
    [0.45, 0],
    [0.15, 1],
    [0.85, 1],
    [0.55, 0],
    [1, 0],
    [1, 0],
];

function jigsaw_points(dim=[1,1], t_step=0.01) =
    bspline_curve(t_step, 2, [for (pt=_jigsaw_points_1) [pt.x*dim.x, pt.y*dim.y]]);

/* linear_extrude(height=10) { */
/*     difference() { */
/*         translate([0, -5]) */
/*             rounded_square([21.8, 20], corner_r=5, $fn=16); */
/*         translate([-1, -10]) square([25, 10]); */
/*         offset(r=0.1) polygon(jigsaw_points([21.8, 10])); */
/*     } */

/*     * translate([24.1, 1]) { */
/*         polygon([[0, -1], each jigsaw_points([21.8, 10]), [21.8, -1]]); */
/*     } */
/* } */

/* * translate([35, 0, 0]) rotate([0, 0, 180]) 2020latch(10); */
