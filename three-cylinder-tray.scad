d = 33;
h = 46;
round = d * 0.15;
precision = d * 0.01;

r = d/2;

$fn=64;

linear_extrude(h) {
    offset(r=-round) {
        offset(r=precision+round) {
            for (delta = [[-r, 0], [r, 0], [0, sqrt(3)*r]]) {
                translate(delta) {
                    circle(r=r);
                }
            }

            mid_r = (sqrt(3)-1)*r*0.8;
            translate([0, mid_r]) {
                circle(r=mid_r);
            }
        }
    }
}
