d = 5;
h = 45;
h2 = 25;
xy = [25, 40];
pocket_depth = 10;

overlap = 0.5;
d2 = d/2;

translate([-d2, -d2, 0]) {
    difference() {
        hull() {
            cube([xy.x, xy.y+d2, h]);
            cube([d2, xy.y+d2, h+h2]);
        }

        translate([d2, d2, h]) {
            cube([xy.x+overlap, xy.y+overlap, h2+overlap]);
        }

        /* rounded corners */
        for(xx=[0,1], yy=[0,1]) {
            translate([d/2 + xx*(xy.x-d), d/2 + yy*(xy.y-d2), -overlap]) {
                hh = h+h2+overlap*2;
                difference() {
                    translate([(xx-1)*d, (yy-1)*d, 0]) {
                        cube([d, d, hh]);
                    }
                    translate([0, 0, -overlap]) {
                        cylinder(d=d, h=hh+2*overlap, $fn=16);
                    }
                }
            }
        }

        /* screwheads on back */
        translate([d2, d2, h-pocket_depth]) {
            linear_extrude(height=pocket_depth+overlap) {
                hull() {
                    for ( y=[10, 30]) {
                        translate([10, y]) {
                            circle(d=10, $fn=24);
                        }
                    }
                }
            }
        }

        /* side screwholes */
        for(y=[10, 30]) {
            translate([-overlap, y+d2, h+20]) {
                rotate([0, 90, 0]) {
                    cylinder(d=4.2, h=d+2*overlap, $fn=16);
                }
            }
        }
    }
}
