use <dotSCAD/src/rounded_square.scad>;

cutout_a = 45;
cutout_corner_r = 6; // 9.5;
cutout_delta = [-4.2, -1.64];

front_a = 50;
front_corner_r = 2.4;

bottom_outer_h = 8.8;
bottom_inner_h = 7;
bottom_dh = bottom_outer_h - bottom_inner_h;
bottom_inner_dh = 2; // 5.5
bottom_inner_offset_r = 1;
bottom_thickness = 2;

bottom_part_h = 1.64;

peg_a = 28;
peg_d = 2.25; // 1.75mm filament + margin

show_metadata = false; // whether to print dimension on bottom part

module socket_cutout(offset_r=0.16) {
    offset(r=offset_r) {
        rounded_square(cutout_a, corner_r=cutout_corner_r, center=true, $fn=36);
    }
}

module frame_inside(offset_r=-0.67) {
    offset(r=offset_r) {
        difference() {
            square(68, center=true);
            projection(cut=true) translate([-4.2, -1.3, -4]) import("vendor/tngo_frame.stl");
        }
    }
}

module bottom_part(height=bottom_part_h) {

    difference() {
        linear_extrude(height=height) {
            difference() {
                frame_inside();

                rounded_square(56, corner_r=6, center=true, $fn=24);

                for (x=[-peg_a, peg_a], y=[-peg_a, peg_a]) {
                    translate([x, y]) {
                        circle(d=peg_d, $fn=12);
                    }
                }
            }
        }

        if (show_metadata) {
            full_mm = floor(height);
            tenth_mm = floor((height - full_mm + 0.005) * 10);
            echo(full_mm);
            echo(tenth_mm);
            for (i=[1:full_mm]) {
                translate([-25+3*i, 30, height-0.6]) cylinder(d=2, h=1.1, $fn=12);
            }
            if (tenth_mm > 0) {
                for (i=[1:tenth_mm]) {
                    translate([-25+3*full_mm+2.5*(i+1), 30, height-0.6]) cylinder(d=1.4, h=1.1, $fn=12);
                }
            }
        }
    }
}

/* top */
difference() {
    linear_extrude(height=bottom_inner_h) {
        difference() {
            frame_inside();
            // offset(r=-5) frame_inside();
            translate(cutout_delta) {
                socket_cutout();
            }
        }
    }

    translate([cutout_delta.x, cutout_delta.y, bottom_thickness]) {
        linear_extrude(height=bottom_inner_h) {
            offset(r=0.28) {
                rounded_square(front_a, corner_r=front_corner_r, center=true, $fn=36);
            }
        }
    }

    hull() {
        translate([0, 0, bottom_inner_h]) {
            linear_extrude(height=0.1) {
                frame_inside(offset_r=-1.5);
            }
        }
        translate([cutout_delta.x, cutout_delta.y, bottom_inner_h - bottom_inner_dh]) {
            linear_extrude(height=0.1) {
                offset(r=bottom_inner_offset_r, $fn=36) square(front_a, center=true);
            }
        }
    }

    for (x=[-peg_a, peg_a], y=[-peg_a, peg_a]) {
        translate([x, y, -0.1]) {
            cylinder(d=peg_d, h=5, $fn=12);
        }
    }
}
