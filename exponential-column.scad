include <_common.scad>;

module exponential_column(h, r1, r2, d1, d2, steps) {
    assert(r1 != undef || d1 != undef, "r1 or d1 has to be defined");
    assert(!(r1 == undef && d1 == undef), "r1 and d1 cannot be both defined");
    assert(r2 != undef || d2 != undef, "r2 or d2 has to be defined");
    assert(!(r2 == undef && d2 == undef), "r2 and d2 cannot be both defined");

    _r1 = r1 == undef ? d1/2 : r1;
    _r2 = r2 == undef ? d2/2 : r2;
    _steps = steps == undef ? ceil(h/$layer) : steps;

    step = h/_steps;
    lmbd = ln(step/h) / (_r2-_r1);

    for (i=[1:_steps]) {
        y = i*step;
        x1 = ln(y/h)        / (-lmbd);
        x2 = ln((y+step)/h) / (-lmbd);

        translate([0, 0, y-step]) {
            cylinder(r1=_r2+x1, r2=_r2+x2, h=step);
        }
    }
}

module maybe_exponential_column(h, r1, r2, d1, d2, steps) {
    if ($draft) {
        cylinder(h=h, r1=r1, r2=r2, d1=d1, d2=d2);
    } else {
        exponential_column(h=h, r1=r1, r2=r2, d1=d1, d2=d2, steps=steps);
    }
}

