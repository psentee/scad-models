use <dotSCAD/src/rounded_square.scad>;

use <parts/limit-switch.scad>;

dim = [23, 20, 5.5];
x0 = 6-dim.x;

corner_r = 2.5;

difference() {
    union() {
        linear_extrude(height=dim.z) {
            translate([x0, 0]) {
                rounded_square([dim.x, dim.y], corner_r=corner_r, $fn=16);
            }
        }
        translate([x0, 7.2, 0]) {
            cube([dim.x, 5.6, dim.z+5]);
        }
    }

    for(y=[5, 14]) {
        translate([0, y, -1]) {
            cylinder(d=2.5, h=10, $fn=12);
        }
        translate([0, y, dim.z-2.5]) {
            cylinder(d=5, h=10, $fn=24);
        }
    }

    translate([x0+4, 10, 0]) {
        translate([0, 0, -1]) {
            cylinder(d=4.2, h=10, $fn=16);
        }

        translate([0, 0, -0.01]) {
            cylinder(d1=7.5, d2=1.5, h=3, $fn=24);
        }

        translate([0, 0, dim.z]) {
            cylinder(d=12, h=10, $fn=32);
        }
    }

    translate([-26, -10, -.1]) {
        linear_extrude(height=23) {
            rounded_square([20, 16], corner_r=corner_r, $fn=16);
        }
    }

    translate([x0-1, 6-1, -0.1]) {
        difference() {
            cube([corner_r+1, corner_r+1, 23]);
            translate([corner_r+1, corner_r+1, -0.1]) {
                cylinder(r=corner_r, h=23.5, $fn=16);
            }
        }
    }

    translate([-7, -1, -0.1]) {
        difference() {
            cube([corner_r+1, corner_r+1, 23]);
            translate([corner_r+1, corner_r+1, -0.1]) {
                cylinder(r=corner_r, h=23.5, $fn=16);
            }
        }
    }
}

*color("orange") {
    translate([0, 19, 0]) {
        rotate([0, 180, 180]) {
            limit_switch();
        }
    }
}
