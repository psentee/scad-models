rows = 2;
cols = 4;
distance = 2;
tube_d = 4;

module full_shape() {
    difference() {
        square([cols*tube_d + (cols+1)*distance,
                rows*tube_d + (rows+1)*distance]); /* TODO: rounded corners or something */
        for(x=[0:cols-1], y=[0:rows-1]) {
            translate([distance + tube_d/2, distance + tube_d/2] + [x, y]*(tube_d+distance))
                circle(d=tube_d, $fn=16);
        }
    }
}

module center_grid() {
    translate([distance/2, distance + tube_d/2])
        square([cols, rows-1] * (tube_d+distance));
}


difference() {
    full_shape();
    center_grid();
}

translate([(1+cols)*(tube_d+distance), 0])
intersection() {
    full_shape();
    center_grid();
}
