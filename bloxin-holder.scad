bottle_d = 31;
bottle_h = 35;
bridge = [5, 10];
hook_h = 85;
thickness = 1.6;
bottom_thickness = 1.84;
round_r = 5;
top_hole_d = 5;

epsilon = $preview ? 0.01 : 0;
center_x = (bridge.x + bottle_d)/2;
holder_h = bottle_h + bottle_d + thickness + hook_h;
top_round_d = bridge.y+round_r*1.55;

difference() {
    translate([0, 0, -bottom_thickness]) {
        linear_extrude(bottom_thickness+holder_h) {
            offset(r=-round_r, $fn=24) offset(r=round_r) {
                for (x=[-center_x, center_x]) {
                    translate([x, 0]) {
                        circle(d=bottle_d+2*thickness, $fn=48);
                    }
                }
                square([bridge.x+bottle_d, bridge.y], center=true);
            }
        }
    }
    for (xx = [-1, 1]) {
        translate([xx*center_x, 0, 0]) {
            cylinder(d=bottle_d, h=holder_h+0.1, $fn=48);
        }
        translate([xx*(bridge.x/2 + bottle_d + thickness), 0, holder_h-hook_h]) {
            rotate([90, 0, 0]) {
                cylinder(r=bottle_d+thickness, h=bottle_d+2*thickness, center=true, $fn=64);
            }
        }
        translate([xx*(center_x+thickness/2), 0, holder_h-hook_h]) {
            linear_extrude(hook_h+0.1) {
                square([bottle_d+thickness+epsilon, bottle_d+thickness*2+epsilon], center=true);
            }
        }

        translate([0, 0, holder_h-top_round_d/2]) {
            difference() {
                translate([0, 0, top_round_d/4+0.1]) {
                    cube([bridge.x+epsilon, top_round_d, top_round_d/2+0.2], center=true);
                }
                rotate([0, 90, 0]) {
                    cylinder(d=top_round_d, h=bridge.x+2*epsilon, center=true, $fn=32);
                }
            }
        }
    }
    translate([0, 0, holder_h - top_round_d/2]) {
        rotate([0, 90, 0]) {
            cylinder(d=top_hole_d, h=bridge.x+2*epsilon, center=true, $fn=32);
        }
    }
}
