thunderbolt_body = [28.5, 11, 7];
thunderbolt_cable_d = 7;
thunderbolt_cable_l = 15;

magsafe_adapter_body = [10.6, 17.6, 7.7];

magsafe_plug_d = 7;
magsafe_plug_l = 27;
magsafe_plug_dy = 1.2;
magsafe_light_dy = 7.7;

plugs_distance =                /* between plug centers */
    4.25                        /* <- measured distance between plug sides */
    +
    17.6/2                      /* <- 1/2 measured magsafe adapter width */
    +
    11/2                        /* <- 1/2 measured thunderbolt plug width */
    ;

epsilon = 0.01;

$fa = 6;
$fs = 1;

module thunderbolt_plug() {
    translate([0, -thunderbolt_body.y/2, -thunderbolt_body.z/2]){
        cube(thunderbolt_body);
        translate([thunderbolt_body.x - epsilon, thunderbolt_body.y/2, thunderbolt_body.z/2]) {
            rotate([0, 90, 0]) {
                cylinder(d=thunderbolt_cable_d, h=thunderbolt_cable_l+epsilon);
            }
        }
    }
}

module magsafe_plug() {
    echo(magsafe_adapter_body.y/2 - magsafe_plug_dy - magsafe_light_dy);

    translate([0, magsafe_adapter_body.y/2 - magsafe_plug_dy, 0]){
        translate([17.7 - magsafe_plug_d/2, 0, 0]){
            rotate([90, 0, 0]) {
                cylinder(d=magsafe_plug_d, h=magsafe_plug_l);
            }

            /* projection */
            magsafe_projection_y = magsafe_adapter_body.y - magsafe_plug_dy;
            translate([-magsafe_plug_d, -magsafe_projection_y, -magsafe_plug_d/2]) {
                cube([magsafe_plug_d, magsafe_projection_y, magsafe_plug_d]);
            }

            /* light */
            translate([0, -magsafe_light_dy, 0]) {
                cylinder(d=0.5, h=magsafe_plug_d*3, center=true);
            }
        }

        /* adapter */
        translate([0,  magsafe_plug_dy - magsafe_adapter_body.y, -magsafe_adapter_body.z/2]) {
            cube(magsafe_adapter_body);
        }
    }
}

module plugs() {
    magsafe_plug();
    translate([0, plugs_distance, 0]) {
        thunderbolt_plug();
    }
}

plugs();

/* color("blue", alpha=0.8) { */
/*     translate([39, 27, 1]) {    rotate([0, 0, 180]) { */
/*             import("/home/japhy/Downloads/Bottom_Dongle_VD.stl"); */
/*         } */
/*     } */
/* } */
